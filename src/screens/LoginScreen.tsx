import React from 'react';
import {View, Text, Button, TextInput, TouchableOpacity} from 'react-native';

const LoginScreen: React.FC<{navigation: any}> = ({navigation}) => {
  console.log(navigation);
  return (
    <View>
      <TextInput placeholder="Email" />
      <TextInput placeholder="Password" />
      <Button title="Login" onPress={() => navigation.navigate('SignUp')} />
      <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
        <Text>Don't have an account? Press here to Sign Up</Text>
      </TouchableOpacity>
    </View>
  );
};

export default LoginScreen;
