import React from 'react';
import {View, Text, Button, TextInput, TouchableOpacity} from 'react-native';

const SignUpScreen: React.FC<{navigation: any}> = ({navigation}) => {
  return (
    <View>
      <TextInput placeholder="Email" />
      <TextInput placeholder="Password" />
      <Button title="Sign Up" onPress={() => navigation.navigate('SignUp')} />
      <TouchableOpacity onPress={() => navigation.navigate('LogIn')}>
        <Text>Already have an account? Press here to Log In</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SignUpScreen;
